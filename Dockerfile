FROM node
WORKDIR /src
COPY . .
EXPOSE 3300
CMD node server.js